// Item a

let person = {
	firstName: "Juan",
	lastName: "Dela Cruz",
	age: 21,
	address: {
		city: "Quezon City",
		region: "Metro Manila"
	} 
};

function printPersonDetails(person){
	let printPerson = "Hello! I am "+ person.firstName + " " + person.lastName + ", " + "age" + " years old, and currently living in " + person.address.city + ", " + person.address.region + ".";
	console.log(printPerson);
}

printPersonDetails(person);

// Item b

let age = 17;


if (age < 18) {
	answer = "Sorry, You're too young to vote.";
};

console.log(answer);



// Item 1

function item1() {
let month = prompt("Enter a month number:");

if (month > 12) {
	window.alert("Invalid input! Please enter the month number between 1-12.");
}

let numberMonth = parseInt(month);

function checkMonth (numberMonth){
	switch (numberMonth) {
		case 1:
			console.log('Total number of days for January: 31');
			break;
		case 2:
			console.log('Total number of days for February: 28');
			break;
		case 3:
			console.log('Total number of days for March: 31');
			break;
		case 4:
			console.log('Total number of days for April: 30');
			break;
		case 5:
			console.log('Total number of days for May: 31');
			break;
		case 6:
			console.log('Total number of days for June: 30');
			break;
		case 7:
			console.log('Total number of days for July: 31');
			break;
		case 8:
			console.log('Total number of days for August: 31');
			break;
		case 9:
			console.log('Total number of days for September: 30');
			break;
		case 10:
			console.log('Total number of days for October: 31');
			break;
		case 11:
			console.log('Total number of days for November: 30');
			break;
		case 12:
			console.log('Total number of days for December: 31');
			break;
	};
};

checkMonth(numberMonth);
};

// Item 2

function item2 () {

let year = prompt("Enter a year to check:");
let numberYear = parseInt(year);

function leapYear(numberYear){
	if (numberYear % 4 != 0) {
		return false;
	}
	else if (numberYear % 400 == 0) {
		return true;
	}
	else if (numberYear % 100 == 0) {
		return false;
	}
	else {
		return true;
	} 
}


let answerYear = leapYear(numberYear);

function checkYear (answerYear){
	if (answerYear != true) {
		console.log ("Not a leap year!");
	}

	else {
		console.log(numberYear + " is a leap year.");
	}
};

checkYear(answerYear);

}

// Item 3

function item3(){
let enteredNumber = prompt("Enter a number:");
let number = parseInt(enteredNumber);

function counter(number) {
	console.log(number)
	while (number>0){
		number --;
		console.log(number);
	}
}

counter(number);}


// select item to run
let item = prompt("Select item to run. (1) Month Number, (2) Leap Year, (3) Reverse Number Count.");
let numberItem = parseInt(item);
switch (numberItem){
	case 1:
		item1();
		break;
	case 2:
		item2();
		break;
	case 3:
		item3();
		break;
}
